"use strict";

var app = angular.module('twitchAngular', ['ngTable']);

app.controller("demoCtrl",['NgTableParams','$scope','$http','$q', function(NgTableParams,$scope,$http,$q) {

  var self = this;
  $scope.twitchEndPoint = "https://api.twitch.tv/kraken/games/top?client_id=3ayqtffruo2goxf0cvyp75wjm28g4pq&limit=100"
  self.deleteCount = 0;

  // Controller startup tasks
  activate();

  function activate() {

    fetchLatest()
      .then(function(data){

        $scope.originalData = data
          .top
          .map(function(topGames){

            return {
              gameName:topGames.game.name,
              viewers:topGames.viewers
            }
          })

        $scope.cols = columnGen($scope.originalData)
        $scope.cols_nodelete = $scope.cols.filter( column => column.title != "Delete")

        $scope.tableParams = new NgTableParams({
          page : 1,
          count : 10
          },{
          dataset : angular.copy($scope.originalData)
        });

      })
  }

  function fetchLatest() {
    var deferred = $q.defer();

    $http.get($scope.twitchEndPoint)
      .then(
        function(res) {
          deferred.resolve(res.data)
        },
        function(res) {
          deferred.reject(res.textStatus)
        }
      )
      .catch(function(err) {
        deferred.reject(err);
      })

      return deferred.promise;
    }

  function columnGen (obj) {

    var cols = [];

    var typeArr= {string: "text", number:"number"};

    for (var keyname in obj[0]) {

      var type = typeof obj[0][keyname]
      var typeForNgTable = typeArr[type]

      var columnSpec = {
        title : keyname,
        sortable : keyname,
        field : keyname,
        dataType : typeForNgTable,
        show : true,
        filter : { [keyname] : typeForNgTable}
      };

      cols.push(columnSpec);

    };

    cols.push({field: "action",title: "Delete",dataType: "command",show : true})

    return cols;
  };


  $scope.add = function() {
    self.isEditing = true;
    self.isAdding = true;
    $scope.tableParams.settings().dataset.unshift({});

    $scope.tableParams.page(1);
    $scope.tableParams.reload();
  }

  $scope.cancelChanges = function() {
    resetTableStatus();
    var currentPage = $scope.tableParams.page();
    $scope.tableParams.settings({
      dataset: angular.copy($scope.originalData)
    });
    
    if (!self.isAdding) {
      $scope.tableParams.page(currentPage);
    }
  }

  $scope.del = function(row) {
;
    $scope.tableParams.settings().dataset = $scope.tableParams.settings().dataset.filter(myrow => myrow != row)
    self.deleteCount++;
    $scope.tableParams.reload().then(function(data) {
      if (data.length === 0 && $scope.tableParams.total() > 0) {
        $scope.tableParams.page($scope.tableParams.page() - 1);
        $scope.tableParams.reload();
      }
    });
  }

  self.hasChanges = function() {
    return self.tableForm.$dirty || self.deleteCount > 0
  }

  function resetTableStatus() {
    self.isEditing = false;
    self.isAdding = false;
    self.deleteCount = 0;
    self.tableForm.$setPristine();
  }

  $scope.saveChanges = function() {
    resetTableStatus();
    var currentPage = $scope.tableParams.page();
    $scope.originalData = angular.copy($scope.tableParams.settings().dataset);
  }
}]);


